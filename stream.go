// Package stream provides a translation of the python standard library module intertools.
// Many of the functions have been brought over, althought not all.
// In this implementation, chan interface{} has been used as all Streams if more specific types are necessary,
// feel free to copy the code to your project to be implemented with more specific types.
package stream

import (
	"sync"
)

// Stream represent a channel of empty interface.
// This is used to be a pseudo generical type.
type Stream chan interface{}

// MultiMapper is the callback to pass in parameter of MultiMap method.
type MultiMapper func(...interface{}) interface{}

// MapCallback is the callback to pass in parameter of Map method.
type MapCallback func(current interface{}, index int, all Stream) interface{}

// FilterCallback is the callback to pass in parameter of Filter method.
type FilterCallback func(current interface{}, index int, all Stream) bool

// ReduceCallback is the callback to pass in parameter of Reduce method
type ReduceCallback func(memo interface{}, element interface{}) interface{}

// New function create an instance of generics Stream
func New(els ...interface{}) Stream {
	c := make(Stream)
	go func() {
		for _, el := range els {
			c <- el
		}
		close(c)
	}()
	return c
}

// Int64 function create an instance of int64 Stream
func Int64(els ...int64) Stream {
	c := make(Stream)
	go func() {
		for _, el := range els {
			c <- el
		}
		close(c)
	}()
	return c
}

// Int32 function create an instance of int32 Stream
func Int32(els ...int32) Stream {
	c := make(Stream)
	go func() {
		for _, el := range els {
			c <- el
		}
		close(c)
	}()
	return c
}

// Float64 function create an instance of float64 Stream
func Float64(els ...float64) Stream {
	c := make(Stream)
	go func() {
		for _, el := range els {
			c <- el
		}
		close(c)
	}()
	return c
}

// Float32 function create an instance of float32 Stream
func Float32(els ...float32) Stream {
	c := make(Stream)
	go func() {
		for _, el := range els {
			c <- el
		}
		close(c)
	}()
	return c
}

// Uint function create an instance of uint Stream
func Uint(els ...uint) Stream {
	c := make(Stream)
	go func() {
		for _, el := range els {
			c <- el
		}
		close(c)
	}()
	return c
}

// Uint64 function create an instance of uint64 Stream
func Uint64(els ...uint64) Stream {
	c := make(Stream)
	go func() {
		for _, el := range els {
			c <- el
		}
		close(c)
	}()
	return c
}

// Uint32 function create an instance of uint32 Stream
func Uint32(els ...uint32) Stream {
	c := make(Stream)
	go func() {
		for _, el := range els {
			c <- el
		}
		close(c)
	}()
	return c
}

// List function convert a Stream into an array of empty interface
func List(it Stream) []interface{} {
	arr := make([]interface{}, 0, 1)
	for el := range it {
		arr = append(arr, el)
	}
	return arr
}

// Count from i to infinity
func Count(i int) Stream {
	c := make(Stream)
	go func() {
		for ; true; i++ {
			c <- i
		}
	}()
	return c
}

// Cycle through an Streamator infinitely (requires memory)
func Cycle(it Stream) Stream {
	c, a := make(Stream), make([]interface{}, 0, 1)
	go func() {
		for el := range it {
			a = append(a, el)
			c <- el
		}
		for {
			for _, el := range a {
				c <- el
			}
		}
	}()
	return c
}

// Repeat an element n times or infinitely
func Repeat(el interface{}, n ...int) Stream {
	c := make(Stream)
	go func() {
		for i := 0; len(n) == 0 || i < n[0]; i++ {
			c <- el
		}
		close(c)
	}()
	return c
}

// Chain together multiple Streamators
func Chain(its ...Stream) Stream {
	c := make(Stream)
	go func() {
		for _, it := range its {
			for el := range it {
				c <- el
			}
		}
		close(c)
	}()
	return c
}

// DropWhile get a new Stream object without elements described by filter callback
func (it Stream) DropWhile(fn FilterCallback) Stream {
	c := make(Stream)
	go func() {
		i := 0
		for el := range it {
			if !fn(el, i, it) {
				c <- el
				break
			}
			i++
		}
		for el := range it {
			c <- el
		}
		close(c)
	}()
	return c
}

// TakeWhile return a new Stream with elements described by filter callback
func (it Stream) TakeWhile(fn FilterCallback) Stream {
	c := make(Stream)
	go func() {
		i := 0
		for el := range it {
			if fn(el, i, it) {
				c <- el
			} else {
				break
			}
			i++
		}
		close(c)
	}()
	return c
}

// Filter out any elements where pred(el) == false
func (it Stream) Filter(fn FilterCallback) Stream {
	c := make(Stream)
	go func() {
		i := 0
		for el := range it {
			if fn(el, i, it) {
				c <- el
			}
			i++
		}
		close(c)
	}()
	return c
}

// Every test if all element respect callback condition
func (it Stream) Every(fn FilterCallback) chan bool {
	c := make(chan bool)
	go func() {
		i := 0
		for el := range it {
			if !fn(el, i, it) {
				break
			}
			i++
		}
		c <- i != len(it)
		close(c)
	}()
	return c
}

// Some test if one element respect the callback condition
func (it Stream) Some(fn FilterCallback) chan bool {
	c := make(chan bool)
	go func() {
		i := 0
		found := false
		for el := range it {
			if fn(el, i, it) {
				found = true
				break
			}
			i++
		}
		c <- found
		close(c)
	}()
	return c
}

// Slice cut a Stream according to a start, stop and step value
// start (inclusive) to [stop (exclusive) every [step (default 1)]]
func (it Stream) Slice(startstopstep ...int) Stream {
	start, stop, step := 0, 0, 1
	if len(startstopstep) == 1 {
		start = startstopstep[0]
	} else if len(startstopstep) == 2 {
		start, stop = startstopstep[0], startstopstep[1]
	} else if len(startstopstep) >= 3 {
		start, stop, step = startstopstep[0], startstopstep[1], startstopstep[2]
	}

	c := make(Stream)
	go func() {
		i := 0
		// Start
		for el := range it {
			if i >= start {
				c <- el // inclusive
				break
			}
			i++
		}

		// Stop
		i, j := i+1, 1
		for el := range it {
			if stop > 0 && i >= stop {
				break
			} else if j%step == 0 {
				c <- el
			}

			i, j = i+1, j+1
		}

		close(c)
	}()
	return c
}

// Map an Streamator to fn(el) for el in it
func (it Stream) Map(fn MapCallback) Stream {
	c := make(Stream)
	go func() {
		i := 0
		for el := range it {
			c <- fn(el, i, it)
			i++
		}
		close(c)
	}()
	return c
}

// MultiMap p, q, ... to fn(pEl, qEl, ...)
// Breaks on first closed channel
func MultiMap(fn MultiMapper, its ...Stream) Stream {
	c := make(Stream)
	go func() {
	Outer:
		for {
			els := make([]interface{}, len(its))
			for i, it := range its {
				if el, ok := <-it; ok {
					els[i] = el
				} else {
					break Outer
				}
			}
			c <- fn(els...)
		}
		close(c)
	}()
	return c
}

// MultiMapLongest p, q, ... to fn(pEl, qEl, ...)
// Breaks on last closed channel
func MultiMapLongest(fn MultiMapper, its ...Stream) Stream {
	c := make(Stream)
	go func() {
		for {
			els := make([]interface{}, len(its))
			n := 0
			for i, it := range its {
				if el, ok := <-it; ok {
					els[i] = el
				} else {
					n++
				}
			}
			if n < len(its) {
				c <- fn(els...)
			} else {
				break
			}
		}
		close(c)
	}()
	return c
}

// Starmap map a Stream if arrays to a fn(els...)
// If not, Starmap will act like MultiMap with a single Stream
func (it Stream) Starmap(fn MultiMapper) Stream {
	c := make(Stream)
	go func() {
		for els := range it {
			if elements, ok := els.([]interface{}); ok {
				c <- fn(elements...)
			} else {
				c <- fn(els)
			}
		}
		close(c)
	}()
	return c
}

// Zip up multiple Streams into one
// Close on shortest Stream
func Zip(its ...Stream) Stream {
	c := make(Stream)
	go func() {
		defer close(c)
		for {
			els := make([]interface{}, len(its))
			for i, it := range its {
				if el, ok := <-it; ok {
					els[i] = el
				} else {
					return
				}
			}
			c <- els
		}
	}()
	return c
}

// ZipLongest zip up multiple Streams into one
// Close on longest Stream
func ZipLongest(its ...Stream) Stream {
	c := make(Stream)
	go func() {
		for {
			els := make([]interface{}, len(its))
			n := 0
			for i, it := range its {
				if el, ok := <-it; ok {
					els[i] = el
				} else {
					n++
				}
			}
			if n < len(its) {
				c <- els
			} else {
				break
			}
		}
		close(c)
	}()
	return c
}

// Reduce the Stream (aka fold) from the left
func (it Stream) Reduce(fn ReduceCallback, memo interface{}) interface{} {
	for el := range it {
		memo = fn(memo, el)
	}
	return memo
}

// Tee split a Stream into n multiple Streams
// Requires memory to keep values for n Streams
func (it Stream) Tee(n int) []Stream {
	deques := make([][]interface{}, n)
	Streams := make([]Stream, n)
	for i := 0; i < n; i++ {
		Streams[i] = make(Stream)
	}

	mutex := new(sync.Mutex)

	gen := func(myStream Stream, i int) {
		for {
			if len(deques[i]) == 0 {
				mutex.Lock()
				if len(deques[i]) == 0 {
					if newval, ok := <-it; ok {
						for i, d := range deques {
							deques[i] = append(d, newval)
						}
					} else {
						mutex.Unlock()
						close(myStream)
						break
					}
				}
				mutex.Unlock()
			}
			var popped interface{}
			popped, deques[i] = deques[i][0], deques[i][1:]
			myStream <- popped
		}
	}
	for i, Stream := range Streams {
		go gen(Stream, i)
	}
	return Streams
}

// Tee2 helper to tee just into two Streams
func (it Stream) Tee2() (Stream, Stream) {
	Streams := it.Tee(2)
	return Streams[0], Streams[1]
}
